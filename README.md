# Visão Computacional



## O que é o YOLOv5

YOLOv5, ou “You Only Look Once”, é um algoritmo de detecção de objetos baseado em deep learning que se destaca por sua rapidez e eficiência. Ele foi desenvolvido para detectar múltiplos objetos em uma imagem. Essa abordagem se diferencia de outros algoritmos que dividem a tarefa em duas etapas: identificar regiões de interesse e classificar os objetos nessas regiões.

## Detecção de objetos com YOLOv5 no TensorFlow

Com o YOLOv5 no TensorFlow, há um  modelo de rede neural é capaz de reconhecer objetos em diversas categorias, incluindo pessoas, carros, animais e muitos outros. No contexto atual, em que a análise de imagens e vídeos desempenha um papel fundamental em diversas áreas, desde segurança até automação industrial, ter um sistema eficiente de detecção de objetos é essencial.


## Yolo no contexto do OvniTrap

No contexto do projeto, utilizamos o YOLOv5 para reconhecimento de imagem, treinando um dataset personalizado composto por fotos tiradas por nós mesmos através de uma webcam conectada a uma Raspberry Pi no ambiente da armadilha. Para fins demonstrativos, utilizamos pequenos tubos plásticos para simular larvas no ambiente.

Após tirar as fotos, realizamos a anotação manual de cada uma delas.  
 A anotação de cada imagem é um processo de demarcar com um retângulo a posição exata do(s) objeto(s) que se deseja identificar, para isso utilizamos a ferramenta de rotulação presente no [Roboflow](https://roboflow.com/). 

 Uma vez anotadas todas as imagens, é possivel exporta-las para diversos formatos específicos, em nosso contexto utilizamos o formato 'YOLO v5 PyTorch', em que o dataset é divido em imagens e suas labels associadas no formato suportado pelo Yolo.


Com as imagens e suas labels associadas, é possível treinar qualquer modelo de reconhecimento de imagem no YOLOv5. O treinamento requer a especificação da quantidade de classes e do caminho do diretório do dataset a ser treinado. Ao final do treinamento, é gerado um arquivo de pesos personalizados da rede neural chamado 'best.pt'. Esse arquivo permite realizar a detecção utilizando o conhecimento adquirido pela rede neural durante o treinamento com o dataset personalizado.


* Obs:  Inicialmente o modelo rodava localmente na raspberry pi, mas por questões de performance foi necessário realizar o deploy do mesmo para utilizá-lo como serviço durante a execução do fluxo na raspberry pi. A utilização do serviço no fluxo de execução na parte embarcada está disponível [aqui](https://gitlab.com/lappis-unb/fga-pi2/semestre-2024-1/g1/embarcado-ovnitrap/-/blob/develop/src/armadilha.py?ref_type=heads), na função 'processa_foto'.

* Link do deploy: https://universe.roboflow.com/cesar-clipz/ovnitrap





## Pré-requisitos
- Python instalado (recomenda-se Python 3.6+)
- Git instalado (para clonar o repositório )
- Pacotes Python necessários (instalados via `pip`)

## Passos para rodar a detecção de imagem localmente

### 1. Clonar o Repositório YOLOv5

```
git clone https://gitlab.com/lappis-unb/fga-pi2/semestre-2024-1/g1/visao-computacional.git
```

### 2. Instalar Dependências
```
cd yolov5
pip install -r requirements.txt  
```


### 3. Executar Detecção em uma Imagem 
```
 python3 detect.py --weights best.pt --img 640 --conf 0.7 --source suaimagem.png
```
* Obs: Substitua 'imagem.png' pelo caminho do arquivo da imagem de entrada